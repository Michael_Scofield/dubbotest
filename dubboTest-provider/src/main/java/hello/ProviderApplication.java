package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

/**
 * @author liangqq
 */
@SpringBootApplication
@ImportResource("classpath:provider.xml")
public class ProviderApplication  {

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }

}
