package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@RequestMapping("/helloWorld")
    public String helloWorld(Model model) {
		 
		System.out.println("----------》hello World Contorller 已经调用");
		 
        model.addAttribute("message", "Hello World!");
        return "helloWorld";
    }

}
