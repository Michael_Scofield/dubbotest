package hello;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.dubbo.demo.DemoService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Consumer {

    /**
     * 获取远程服务代理
     */
	@Resource
	DemoService demoService;

	@RequestMapping("/helloWorld")
    public String helloWorld(Model model) {
		 
		System.out.println("----------》hello World Contorller 已经调用");
		 
        model.addAttribute("message", "Hello World!");

        // 显示调用结果
        String hello = demoService.sayHello("world");

        // 显示调用结果
        System.out.println( hello );
        
        
        return "helloWorld";
    }

}
